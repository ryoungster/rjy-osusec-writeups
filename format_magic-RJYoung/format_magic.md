# format_magic

For this challenge, we are given an image, but told it has something to do with QR codes.

###### format_magic.jpg

![img](format_magic.jpg)

However, running it through image stenography programs produce no results. No hidden QR codes or text appears. 

The next place to look is within the file itself. Running it through binwalk on the other hand does yield something.

###### binwalk output

```
DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             JPEG image data, JFIF standard 1.01
382           0x17E           Copyright string: "Copyright (c) 1998 Hewlett-Packard Company"
135414        0x210F6         End of Zip archive, footer length: 22
```

We have the end of a zip file. But no beginning. Zip files are supposed to start with the signature `0x04034b50`. Since binwalk could not find it, it has probably being mangled slightly to break the file.
Searching for pieces of it in a hex editor, eventually the pattern `FF 4B 03 04 14 00 01 00 08 00` can be found. Not only does it contain 3 bytes of the signature, but `08 00` are also a defined part of the header.
Changing the `FF` byte to `50` allows the zip file to be found.

And it is password protected.

The password isn't very strong and can just be brute forced by the computer in a reasonable amount of time.
All of this builds up to the big reveal. The promised QR code.

###### QR code

![img](qr.png)

Or at least enough of it. We have around half of the data and the format info. This is actually sufficient to find the original text.

Using the web app [qrazybox](https://merricx.github.io/qrazybox/), we can paint in the known elements of the QR code. After that we just need to find the correct info pattern that matches what we have remaining.
This leaves us with a QR code that looks like this.

###### Expanded QR code

![img](qr2.png)

Reading the QR code using the site then gives us the flag.
