### Secure-Encryptor

This CTF begins with a program file and a place to connect to.
Before we began, were told some of what was going to happen. The flag is encrypted by XORing with a one time pad.

Connecting to it shows its text to be:
```
Here's the flag, I've just hidden it a little bit...
gqPA
```
Ciphertext continues
```
FP5w==
I will encrypt exactly one plaintext for you: abcd
Here is your ciphertext, hopefully you can find the flag!
7wBW
```
Ciphertext continues
```
dodg==
```
The text "abcd" was user input.
It appears to allow the user to submit plaintext to be encrypted with the same OTP as the flag, which is a major security flaw.

This can be confirmed by opening the program with ghidra.
```c
__edflag = get_otp();
uVar2 = get_flag();
pcVar3 = (char *)random_pad(uVar2);
encrypt(pcVar3,__edflag);
printf("Here\'s the flag, I\'ve just hidden it a little bit...\n%s\n",extraout_RAX);
printf("I will encrypt exactly one plaintext for you: ");
scanf("%1024s",local_418);
pcVar3 = (char *)random_pad(local_418);
encrypt(pcVar3,__edflag);
printf("Here is your ciphertext, hopefully you can find the flag!\n%s\n",extraout_RAX_00);
```
The main function shows that the flag and input are first both padded (checking random_pad() shows it is done to 1024 characters), the processed in a function just called encrypt() with the same OTP.
Opening encrypt() reveals exactly what processing is done. First the padded string is run through a built in function called memfrob(), which XORs it with 42. Then it is XORed with the one time pad. Finally it is converted to base 64.

Reversing these steps should give us the flag, however we don't know the the OTP. It can be determined though, since we have both the plaintext and ciphertext for the user input.

The easiest way to do this is though pwntools in python.

This part comes in two steps:
- First: Recieve both ciphertext blocks while sending a known plaintext block that is the length of the input. I sent 1024 0s
- Second: Reverse the processing on the blocks. 
	- First: Convert both out of base64
	- Second: XOR your input by 42 and its ciphertext to get the OTP
	- Third: XOR the flag ciphertext by the OTP and 42
	- (NOTE: On later reflection, these steps can be made more efficiant. The commutative property applies to XOR so the order of the chain of ((input ^ 42 ^ cipher2) ^ cipher1 ^ 42) does not matter. Also 42 can be eliminated as XOR with itself cancels out and, if all 0s is inputed, input can be removed as XOR with 0 results in the other input.)

In python only integers can be XORed, so this process does involve iterating over the byte objects and then reconverting them to text.

After all that is done, it results in a block of text which contains the flag: osu{d0n7_u5e_4_1_71m3_p4d_m0r3_th4n_0nc3}
