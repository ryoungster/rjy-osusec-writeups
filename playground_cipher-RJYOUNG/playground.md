# playground_cipher

For this challenge, we're given a port to connect to and the python script behind it. It is a cipher based off the Sarah2 cipher, and combines shuffling, multiple rounds, and a substitution table. To help solve it, we're given 1000 runs of the cipher on the sbox of the specific encrypted flag.

#### Examination

###### Code running:

```
Please authenticate by providing the decryption of the following: 
dazhxxeuvjwqajj__tmtmxbsbipy_qfyeqgxjlniq_uvcbtvldyfrr_qynqnkbxfdg

$ aaa
Incorrect... Provided value encrypts to:
aaa

$ bbb
Incorrect... Provided value encrypts to:
oio

$ ccc
Incorrect... Provided value encrypts to:
hlh
```


Running the code, we learn the flag is 66 characters long, which is far to long for brute forcing. We can also see some patterns emerge in short strings.

Examining the code, we can notice some things about three character inputs:

- When encrypted string "ABC" 's cipher text is sbox(B+C) sbox(A) sbox(A+B)

	Playing around with the numbers in this yeilds:

	- The first character of the string directly maps through the substitution table to the center character of its cipher text.
	- Since the index of a is 0, a string of the form "XaX" will map to a string of the from "YYY" where Y is the result of X being put through the substitution table.

Those last two observations can be combined into a slide attack. Since we know that an "XaX" will encrypt to a "YYY", if we find a pairing of those two, their cipher texts after the same number of rounds (which always the case for strings of the same length) will also have the same relationship. These pairings can then be analysed to derive the original substitution table.

###### Retrived XaX & YYY data:

```
XaX
('a', 'i')	('b', 'o')	('c', 'f')	
('d', 'w')	('e', 's')	('f', 'j')	
('g', 'p')	('h', 'l')	('i', 'q')	
('j', 'k')	('k', 'i')	('l', 't')	
('m', 'a')	('n', 'q')	('o', 't')	
('p', 'd')	('q', 'u')	('r', 'r')	
('s', 'p')	('t', 'x')	('u', 'd')	
('v', 'r')	('w', 'c')	('x', 'd')	
('y', 'r')	('z', 'e')	('_', 'p')	
YYY
('a', 'a')	('b', 'i')	('c', 'e')	
('d', 'x')	('e', 'y')	('f', 'w')	
('g', 'k')	('h', 'a')	('i', 'm')	
('j', '_')	('k', 'v')	('l', 'f')	
('m', 'p')	('n', '_')	('o', 'a')	
('p', 'c')	('q', 's')	('r', 'j')	
('s', 'i')	('t', 's')	('u', 'i')	
('v', 'u')	('w', 's')	('x', 'v')	
('y', 'j')	('z', 'r')	('_', 'b')	
```

In this dataset we know that every pair of values in the XaX table will map to a pair in the YYY table. We can then use a recursive algorithm to find a valid substitution table. The procedure is as follows.

#### Algorithm

1. Guess that a currently unmatched value maps to a currently unmatched to value in the substitution.

	> (Example) \
	> Current guess box: \
	> a -> a

```py
def findBox(x='a', gBox = None):
	validY = (y for y in ALPHABET if not in gBox.values())
	for y in validY: # For all values not already pointed to
		localBox = dict(gBox) # Copy gBox to not edit original
		localBox = add(x,y,localBox)
		...
	return localBox
```

2. Attempt to match their other values in the "XaX" and "YYY" table.

	> XaX(a) = i, YYY(a) = a

	> Current guess box: \
	> a -> a \
	> i -> a
	
```py
def addNew(x,y,gBox):
	localBox = dict(gBox)
	...
		localBox[x] = y
		...
			localBox = addNew(XaX[x],YYY[y],localBox)
	...
	return localBox
```

3. Repeat step two until one of those values is already in the guess box, then check if both match the existing box. 

	- If it does, keep the current box and return to step 1 to guess again.
	
	- If it does not, get rid of the current guess box since the last guess and return to step 1.
	
	> a -> a != i -> a 
	
	> Current guess box: \
	> (Empty)
	
```py
def addNew(x,y,gBox):
	localBox = dict(gBox)
	if x not in link.keys() and y not in link.values():
		localBox[x] = y
		...
	elif gBox.get(x) == y # If the pairing is correct
		localBox = findMap(next(x for x in ALPHABET if not in gBox.keys()),localBox)
	else: # Else the pairing is incorrrect
		return gBox
	return localBox
```

4. When, the guess box is full and successfully passes step 3, format it properly and attempt to decrypt flag. After that, send the decryption attempt to the server for validation.

	- If the server confirms it, the flag is found!
	
	- If the server denies it, get rid of the guess box since the last guess and return to step 1.

```py
def addNew(x,y,gBox):
	localBox = dict(gBox)
	if x not in link.keys() and y not in link.values():
		localBox[x] = y
		if len(localBox) == A_LEN: # If the box has reached its length
			if not tryBox(fixBox(localBox)): # If the box fails
				return gBox 
		else:
			localBox = addNew(XaX[x],YYY[y],localBox)
	...
	return localBox # Full box will be returned here
```

After the server has confirmed the flag, the flag has been confirmed. You have it. No more is needed.

#### Extra Remarks

This method uses 51 runs of the cipher retrieve its data (aaa is in both) and a variable but usually(TM) small amount to crack the substitution. The biggest advantage of it is that the 51 data calls are all very small, only 3 characters each, meaning that 3 test flags are actually larger than the entire data pool.

The verification through calling the cipher server in step 4 is needed because it is possible to generate multiple substitution tables that are valid for the same set of data. The only way to know for certain the result of the algorithm is correct is by testing it. 

A more labour intensive alternative would be seeing if the output is human readable, as CTF League flags usually are English text with some 1337 5P34K substitutions, but this is not always the case for CTF flags in general.

A previous iteration of my solution handled this problem through suicide and rebirth.

```py
...
else:
	# Sometimes it just doesn't work
	# Run it again
	p.close()
	import os
	import sys
	os.execv(sys.executable, ['python'] + sys.argv)
```
