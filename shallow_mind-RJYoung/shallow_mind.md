﻿# shallow_mind

For this challenge we are given a website and the code behind it. The site offers a access to an image classifier.

###### The Site

![img](1.png)

Looking at the code, the website will return the flag if the model makes an incorrect prediction. However, it will only make a prediction when given an admin code.

###### The Flag

```py
#server.py
async def classify(request: Request) -> HTTPResponse:
	body = request.json
	...
	if 'admin' in body:
		if body['admin'] == app.config.PASSCODE:
			if 'pic' in body:
				loop = asyncio.get_event_loop()
				answer = await loop.run_in_executor(None, BigBadMLCheck, body['pic'])
				if answer == 'flag':
					return text(app.config.FLAG)
	...
	
#model.py
if (answer == 'red' and (greens > reds)) or (answer == 'green' and (reds > greens)):
	answer = 'flag'
```

We do not know the code, but there is another section of code where it is mentioned.

###### The Vunlerability

```py
# Test auth route
@app.post("/authtest")
def authTest(request: Request) -> HTTPResponse:
	# TODO: delete route, it's useless
	body = request.json
	if 'admin' in body:
	if hash(body['admin']) == hash(app.config.PASSCODE):
		return text('yeah boii')
	# only return dev logic if we're verified dev
	assert body['devcode'] == app.config.DEVCODE
	raise ServerError(f"{body['admin']} is incorrect", status_code=401,quiet=False,context=app_context)
```

Given a "devcode" we can fire off an error, and elsewhere in the code we learn what it is.

```py
# TODO: add devcode
app.config.DEVCODE = None
```

Sending out a post request the section described in that code with the devcode and a dummy admin code gives us back the full error.


```
⚠️ 401 — Unauthorized
=====================
password123 is incorrect

Context
	...
    _LOGO: ""
    PASSCODE: "yxB5X7{G(<,:;kJR"
    _init: "True"
	...
```

Which includes the passcode "yxB5X7{G(<,:;kJR".

All that is left to do is send off an image pretty balanced in both red and green.

![img](red.png)

```
red
```

And if the first image fails, slightly tipping the scales will trick the model, yielding the flag.

![img](Blue.png)

```
osu{flag}
```
