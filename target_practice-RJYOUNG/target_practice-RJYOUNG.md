﻿### target_practice

This CTF begins with a username, specifically "anonhunter26". The goal is given as finding the person's real name, which becomes the flag.

Using an online tool called whatsmyname.app to search for where that username is present, we find he is only present on Twitter.com: https://twitter.com/anonhunter26/with_replies

![img](1.png)

He seams to really enjoy not being found, and he almost no information about him self on it, however he did make one slip. He names @hatebav2ropc as his co-worker.

His social media is similarly sparse, but going back to whatsmyname.app, we can find there is also a GitHub with that username.

![img](2.png)