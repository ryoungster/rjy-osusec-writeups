This CTF begins with a link to a web page which looks like this:

![img](img/1.png)

The text immediately guides the user into the inspect element, where this JavaScript can be found:
```javascript
function get\_sha256(password) {
    return sjcl.codec.hex.fromBits(sjcl.hash.sha256.hash(password));
}

function check\_password() {
    let password = document.getElementById("password").value;
    let hash = get\_sha256(password);
    if (hash == "b0fef621727ff82a7d334d9f1f047dc662ed0e27e05aa8fd1aefd19b0fff312c") {
        document.getElementById("login").submit();
    }
    else {
        document.getElementById("wrong\_label").style.transform = "translate(-50%, -50%) scale(100%)";
        document.getElementById("wrong\_label").style.opacity = "100%";
        setTimeout(() => {
            document.getElementById("wrong\_label").style.transform = "translate(-50%, -50%) scale(75%)";
            document.getElementById("wrong\_label").style.opacity = "0%";
        }, 3000);
    }
}
```
This reveals the hash of the correct password to the site. After that this page can be finished by using the link to "CrackStation" to reverse the sha256 hashing on the password, finding it as “pineapple”.

After submitting the password the UI disappears, an alert pops up saying “Catch me if you can!”, and flashing text begins moving around the screen.
Opening inspect element the function “print\_flag()” can be identified as the action that will occur when the text is clicked. Running it in the console changes the flashing text to the flag “[osu{p1n34ppl3_h45h_Br0wN5_4r3_g00D}]()”

After that the text simply needs to be selected (while still flashing and spinning around) and submitted.

![img](img/2.png)


