### ULTRASECURE

This CTF begins with a program file and a web address.

Opening the address in a browser shows its text to be:
```
Prove you are not human, repeat this to me in less than .05s: 56116
```
Then the website kicks the browser out. Repeating this process shows that the number changes but the result remains the same.

Maybe we should look at the program instead. 

Opening it in ghidra, it can be quickly understood how it works. 

It relies mostly on a function called password_check(). Examining it confirms the earlier observed behaivor.
```C
void password_check(void)
{
...
  local_10 = rand();
  gettimeofday(&local_28,(__timezone_ptr_t)0x0);
  printf("Prove that you are not human, repeat this to me in less than .05s: %d\n",(ulong)local_10);
  __isoc99_scanf(&DAT_00400b9f,&local_40);
  gettimeofday(&local_38,(__timezone_ptr_t)0x0);
  local_18 = ((local_38.tv_sec - local_28.tv_sec) * 1000000 + local_38.tv_usec) - local_28.tv_usec;
  if (50000 < local_18) {
    puts("Whoops, too slow");
    exit(-0x60);
  }
...
```
The program damands that a randomly generated number is returned to it in a very short period of time. Something a human can't do, so a tool is required.

The rest of the program reveals the second part of the program. After the random number is returned it requests a passcode, which is thankfully also in the program. The correct passcode then calls a "print_flag()" function to be run, which is the end goal.
```C
...
  local_c = 0xdeadb33f;
...
...
  puts("You passed the nonce check! Now, Unlock the UltraSecure(tm) Vault:");
  __isoc99_scanf(&DAT_00400b9f,&local_3c);
  if (local_c == local_3c) {
    print_flag();
    exit(0);
  }
...
}
```

These two requirements can be fulfilled using pwntools in python. The first number can be recieved and sent back, then the integer form of deadb33f can be sent back.

This results in the address sending the flag: osu{d3c0mp1ler_go_brrrr}
